'use strict'

const jwt = require('express-jwt')

module.exports = function (app) {
    app.use(jwt({
        secret: process.env.SECRET,
    }).unless({
        path: [
            { url: '/' },
            { url: '/users', methods: ['POST'] },
            { url: '/login', methods: ['POST'] }
        ]
    }))

    app.use(function (err, req, res, next) {
        if (err.name === 'UnauthorizedError') {
            res.status(401).send({ message: 'Invalid token' });
        }
    })
}
