require('dotenv').config()

const express = require('express'),
      app = express(),
      port = process.env.PORT || 3000,
      bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

require('./auth')(app)
require('./mongoose')(app)
require('./api/routes')(app)

app.route('/').get(function (req, res) {
    var showdown = require('showdown'),
        converter = new showdown.Converter(),
        text = String(require('fs').readFileSync('./README.md'))
        html = converter.makeHtml(text)
    // console.log({text})
    res.send('<html><head><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.min.css"></head><body style="max-width: 900px !important; margin: 10px auto;"><div class="markdown-body">' + html + '</div></body></html>')
})




app.use(function (req, res) {
    res.status(404).send({ url: `${req.originalUrl} not found` })
})

app.listen(port)

console.log(`Address Book RESTful API server started on port ${port}`)
