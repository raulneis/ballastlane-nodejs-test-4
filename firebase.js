/**
 * This is a Firebase wrapper, so we don't have code that
 * hits Firebase in controllers
 */
'use strict'

const firebase = require('firebase')

let firebaseDatabase = null

/**
 * Gets a firebase database reference instance
 */
function getFirebaseDatabase() {
    if (firebaseDatabase) {
        return firebaseDatabase
    }

    let config = {
        apiKey: process.env.FIREBASE_KEY,
        authDomain: process.env.FIREBASE_DOMAIN,
        databaseURL: process.env.FIREBASE_DATABASE_URL
    }

    firebase.initializeApp(config)

    return firebaseDatabase = firebase.database()
}

/**
 * Deletes all null/undefined props (so we don't send them to firebase)
 */
function cleanContact(contact) {
    var props = Object.getOwnPropertyNames(contact);
    for (var i = 0; i < props.length; i++) {
        var prop = props[i];
        if (contact[prop] === null || contact[prop] === undefined) {
            delete contact[prop];
        }
    }
    return contact
}

exports.addContact = async function(userId, contact) {
    let newContactKey = getFirebaseDatabase().ref().child('contacts').push().key
    let updates = {}

    contact = cleanContact(contact)

    updates[`/contacts/${newContactKey}`] = contact
    updates[`/user-contacts/${userId}/${newContactKey}`] = contact

    await firebase.database().ref().update(updates)

    return newContactKey
}

exports.getContacts = async function(userId) {
    let ref = `/user-contacts/${userId}`
    let snapshot = await getFirebaseDatabase().ref(ref).once('value')
    let contacts = snapshot.val()

    // contacts should have their ID inside (_ is mongo style, to match users API)
    if (contacts) {
        for (let k of Object.keys(contacts)) {
            contacts[k] = {
                _id: k,
                ...contacts[k]
            }
        }
    }

    // just return values (since firebase returns an object with IDs as keys)
    return contacts ? Object.values(contacts) : []
}

exports.getContact = async function(userId, contactId) {
    let ref = `/user-contacts/${userId}/${contactId}`
    let snapshot = await getFirebaseDatabase().ref(ref).once('value')
    let contact = snapshot.val()

    // contact should have its ID inside (_ is mongo style, to match users API)
    if (contact) {
        contact._id = contactId
    }

    return contact
}

exports.updateContact = async function(userId, contactId, contact) {
    let updates = {}

    updates[`/posts/${contactId}`] = contact
    updates[`/user-posts/${userId}/${contactId}`] = contact
    await await getFirebaseDatabase().ref().update(updates)

    return contactId
}
