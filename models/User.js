'use strict'

const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      bcrypt = require('bcrypt'),
      SALT_WORK_FACTOR = 10

var UserSchema = new Schema({
    email: {
        type: String,
        required: 'Email is required',
        index: { unique: true }
    },
    name: {
        type: String
    },
    password: {
        type: String,
        required: 'Password is required'
    }
})

const saveHook = function (next) {
    var user = this

    if (!user.isModified('password')) return next()

    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) {
            return next(err)
        }

        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) {
                return next(err)
            }
            user.password = hash
            next()
        })
    })
}

UserSchema.pre('save', saveHook)
UserSchema.pre('update', saveHook)

UserSchema.methods.comparePassword = function (password) {
    
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, this.password, function (err, match) {
            if (err || !match) {
                reject(err)
            }
            resolve()
        })
    })
}

module.exports = mongoose.model('Users', UserSchema)
