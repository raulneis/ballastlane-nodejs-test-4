'use strict'

const mongoose = require('mongoose'),
      User = mongoose.model('Users'),
      jwt = require('jsonwebtoken')

exports.list = function (req, res) {
    User.find({}, function (err, users) {
        if (err) {
            res.status(422).json(err)
        } else {
            res.json(users)
        }
    })
}

exports.store = function (req, res) {
    var user = new User(req.body)
    user.save(function (err, user) {
        if (err) {
            res.status(422).json(err)
        } else {
            res.status(201)
                .header('Location', `/users/${user._id}`)
                .json()
        }
    })
}

exports.update = function (req, res) {
    let userId = req.params.userId

    if (!userId || userId === 'me') {
        userId = req.user._id
    }

    User.findOneAndUpdate({ _id: userId }, req.body, { new: true }, function (err, user) {
        if (err) {
            res.status(422).json(err)
        } else {
            res.status(204).json()
        }
    })
}

exports.show = function (req, res) {
    let userId = req.params.userId
    if (!userId || userId === 'me') {
        userId = req.user._id
    }

    User.findById(userId, function (err, user) {
        if (err) {
            res.status(422).json(err)
        } else {
            res.status(200).json(user)
        }
    })
}

exports.login = function (req, res) {

    User.findOne({ email: req.body.email }, async function (err, user) {
        if (err) {
            res.status(422).json(err)
        }

        try {
            await user.comparePassword(req.body.password)

            var token = jwt.sign({ _id: user._id }, process.env.SECRET, {
                expiresIn: 3600
            })

            res.status(200).json({ token })
        } catch (e) {
            res.status(403).json({ message: 'Bad credentials' })
        }
    })
}
