'use srtict'

const firebase = require('../firebase')

exports.list = async function (req, res) {
    let userId = req.params.userId
    if (!userId || userId === 'me') {
        userId = req.user._id
    }
    let contacts = await firebase.getContacts(userId)

    res.status(200)
        .json(contacts)
}

exports.show = async function (req, res) {
    let userId = req.params.userId
    if (!userId || userId === 'me') {
        userId = req.user._id
    }
    let contactId = req.params.contactId

    let contact = await firebase.getContact(userId, contactId)
    if (!contact) {
        res.status(404)
            .json({
                message: 'Contact not found'
            })
    }

    res.json(contact)
}

exports.store = async function (req, res) {
    let userId = req.params.userId
    if (!userId || userId === 'me') {
        userId = req.user._id
    }

    // TODO this fields should be validated
    let { name, email, phone } = req.body
    let contactId = await firebase.addContact(userId, { name, email, phone })

    res.status(201)
        .header('Location', `/users/${userId}/contacts/${contactId}`)
        .json()
}

exports.update = async function (req, res) {
    let contactId = req.params.contactId
    let userId = req.params.userId
    if (!userId || userId === 'me') {
        userId = req.user._id
    }
    let contact = await firebase.getContact(userId, contactId)
    if (!contact) {
        res.status(404)
            .json({
                message: 'Contact not found'
            })
    } else {
        await firebase.updateContact(userId, contactId, req.body)

        res.status(204)
            .json()
    }
}
