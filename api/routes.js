'use strict'

const users = require('./users'),
      contacts = require('./contacts')

module.exports = function (app) {

    app.route('/login')
        .post(users.login)
    
    app.route('/users')
        .get(users.list)
        .post(users.store)

    app.route('/users/me')
        .get(users.show)

    app.route('/users/:userId')
        .get(users.show)
        .put(users.update)

    app.route('/users/me/contacts')
        .get(contacts.list)
        .post(contacts.store)
    
    app.route('/users/:userId/contacts')
        .get(contacts.list)
        .post(contacts.store)
    
    app.route('/users/:userId/contacts/:contactId')
        .get(contacts.show)
        .put(contacts.update)
}
