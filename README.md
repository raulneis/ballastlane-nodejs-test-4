# Ballastlane Nodejs test by Raúl Neis

There is a live version at [Heroku](https://raulneis-ballastlane-test.herokuapp.com/).

Source code is at [GitLab](https://gitlab.com/raulneis/ballastlane-nodejs-test-4). The other 3 questions are [here](https://gitlab.com/raulneis/ballastlane-nodejs-test).

There is `.env.dist` file which should be used as a template for creating a `.env` file with the right settings. As an alternative, environment variables can be set to do the same.

Install dependencies running `npm install`.

Run the project with `node server.js`.

# API

- All data is JSON formatted, both for requests and responses. In the examples below, the `Accept` and `Content-Type` are omitted for brevity.
- For every error response (HTTP 4xx), a `message` fields is included indicating the error.
- Unless stated, all endpoints require authentication (a `Authorization` header, like `Authorization: Bearer <JWT>`).
- For testing propose, there is an user with email `admin@admin.com` and password `admin123`.

## Available endpoints are:

*In every endpoint, `:userId` can be replaces by the string `me` to get the currently authenticated user info.*

### Login

`POST /login`

### Users

`POST /users`

`GET /users`

`GET /users/:userId`

`PUT /users/:userId`

### Contacts

`GET /users/:userId/contacts`

`POST /users/:userId/contacts`

`GET /users/:userId/contacts/:contactId`

`PUT /users/:userId/contacts/:contactId`

___

## Login

*This endpoint, for obvious reasons, does not require authentication*

In order to login, a HTTP POST should be sent to `/login`, with the email and password of the authenticating user.

```json
POST /login

{"email":"admin@admin.com", "password": "admin123"}
```

The API returns `200 OK` if the auth process when OK. In the response body a `token` field is included with a JWT token to use in further requests.

```json
HTTP 200 OK

{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzI0ZDUwMjBkNTRlYzAwMjI3NDBlNzYiLCJpYXQiOjE1NDU5NDk5NzYsImV4cCI6MTU0NTk1MzU3Nn0.hhfWxukQ2ed80XxVMc_7s4P5NYnh6ZW9A7ys032ExDk"
}
```

If the auth parameters (email or password) are incorrect, a `403 FORBIDDEN` status code is return with a `message` fields on the body, detailing the error (in this case it's just 'Bad credentials').

```json
HTTP 403 FORBIDDEN

{
    "message": "Bad credentials"
}
```

___

## User creation/register

*This endpoint does not require authentication (since users must register before authenticating)*

To register a new user:

```json
POST /users

{"email": "email@admin.com", "password": "lorempassword"}
```

Optionally a `name` field can be included.

The API will return a `Location` header pointing to the new resource location. The response body is empty.

```json
HTTP 201 CREATED

Location: /users/5c24d5020d54ec0022740e76
```

If there is an error, a `422 UNPROCESSABLE ENTITY` is return, with the proper error message.

___

## Get users info

To get the whole list of users:

```json
GET /users
```

Returns an array of users:

```json
HTTP 200 OK

[
    {
        "_id": "5c24d5020d54ec0022740e76",
        "email": "admin@admin.com",
        "password": "$2b$10$ANzL2A2ckgQBfQf9gufgS.4zau8TG1EzdqdRUi6lNqw/XfsbU66Ni",
        "name": "Admin"
    },
    ... // more users
]
```

To get an individual user data, a `GET` request to `/users/:userId` returns the user info.

```json
GET /users/5c24d5020d54ec0022740e76
```

Returns the user representation:

```json
HTTP 200 OK

{
    "_id": "5c24d5020d54ec0022740e76",
    "email": "admin@admin.com",
    "password": "$2b$10$ANzL2A2ckgQBfQf9gufgS.4zau8TG1EzdqdRUi6lNqw/XfsbU66Ni",
    "name": "Admin"
}
```

*This response include the password field because of mongodb. A more polished version of the API should strip those fields out*

If the user does not exists, a `404 NOT FOUND` status is returned.

___

## Edit users

To edit a user:

```json
PUT /users/5c24d5020d54ec0022740e76

{
    "email": "email@admin.com",
    ... // other fields to update
}
```

The fields is the request can be `email`, `password` and/or `name` (all optional).

If there is an error on the response a `422 UNPROCESSABLE ENTITY` is returned. 

If the user does not exists, a `404 NOT FOUND` status is returned.

Otherwise a `204 NO CONTENT` is returned, with an empty body.

Note that the API does not check which user can update what, so more checks should be implemented here.

## Get contacts

To get a user's contact list:

```json
GET /users/:userId/contacts
```

Returns a list of contacts:

```json
HTTP 200 OK

[
    {
        "_id": "-LUkTaDCGOsv6T9flyiF",
        "email": "email@gmail.com",
        "name": "Contact Name",
        ... // other contact data
    },
    ... // more contacts
]
```

If the user does not exists, a `404 NOT FOUND` status is returned.

To get an individual contact info:

```json
GET /users/:userId/contacts/:contactId
```

Returns the contact representation:

```json
HTTP 200 OK

{
    "_id": "-LUkTaDCGOsv6T9flyiF",
    "email": "email@gmail.com",
    "name": "Contact Name",
    ... // other contact data
}
```

If the user or the contact does not exists, a `404 NOT FOUND` status is returned.

## Create contacts

To create a new contact:

```json
POST /users/:userId/contacts

{
    "email": "email@gmail.com",
    "name": "Contact name",
    ... // other contact details
}
```

As of this implementation, the fields `email`, `name` and `phone` are accepted.

This endpoint returns `422 UNPROCESSABLE ENTITY` if there is a validation error (which are not yet implemented). Otherwise it returns a `201 CREATED` with a `Location` header pointing to the created contact.

```http
Location: /users/5c24d5020d54ec0022740e76/contacts/-LUlyKcI03YiB30WOrOn
```

## Edit contacts

To edit a contact:

```json
PUT /users/:userId/contacts/:contactId

{
    "email": "email@admin.com",
    ... // other fields to edit
}
```

The fields is the request can be `email`, `name` and/or `phone` (all optional). Non of this is validated.

If there is an error on the response a `422 UNPROCESSABLE ENTITY` is returned. 

If the user or the contact does not exists, a `404 NOT FOUND` status is returned.

Otherwise a `204 NO CONTENT` is returned, with an empty body.

Note, again, that the API does not check which user can update what, so more checks should be implemented here.
