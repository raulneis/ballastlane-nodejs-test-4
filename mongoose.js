'use strict'
const User = require('./models/User'),
      mongoose = require('mongoose')

module.exports = function (app) {
    mongoose.set('useCreateIndex', true)
    mongoose.Promise = global.Promise
    mongoose.connect(process.env.DATABASE_CONNECTION, { useNewUrlParser: true })
}